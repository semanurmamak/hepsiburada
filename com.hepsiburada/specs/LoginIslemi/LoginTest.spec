Hepsiburada Senaryo
=====================
Created by testinium on 14.01.2021

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
Giris Yap
----------------
tags:girisYap
* hepsiburada.com sitesinin açıldığını kontrol et
* Giriş yap ekranını aç
* Giriş yap ekranının açıldığını kontrol et
* "hepsiburada10@gmail.com" ve "Test.1234" ile giriş yap
* Doğru kullanıcı oluğunu kontrol et

