import com.hepsiburada.webAutomation.helper.ElementHelper;
import com.hepsiburada.webAutomation.helper.StoreHelper;
import com.sun.xml.internal.bind.v2.model.core.ElementInfo;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class StepImplementation extends BaseTest {

    BaseMethods baseMethods;
    private static Logger logger = LoggerFactory.getLogger(StepImplementation.class);

    public StepImplementation() {
        baseMethods = new BaseMethods(driver);
    }

    @Step("<seconds> sn kadar bekle")
    public void bekle(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step({"<key> elementine tikla"})
    public void clickElement(String key) {

        bekle(1);
        baseMethods.clickElement(key);
    }

    @Step({"<key> elementine <text> degerini yaz"})
    public void setTextToKey(String key, String text) {

        WebElement elementInfo = baseMethods.findElementKey(key);
        elementInfo.sendKeys(text);
    }

    @Step("<key> elementi var mı")
    public void checkElement(String key){
        try {
            baseMethods.findElementKey(key);
        } catch (Exception e) {
            Assert.fail("Element bulunamadı.");
        }
    }

    @Step("<text> değerinin sayfa üzerinde olup olmadığını kontrol et.")
    public void getPageSourceFindWord(String text) {
        Assert.assertTrue(text + " sayfa üzerinde bulunamadı.",
                driver .getPageSource().contains(text));
        logger.info(text + " sayfa üzerinde bulundu.");
    }
}
