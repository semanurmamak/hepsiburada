import com.hepsiburada.webAutomation.helper.ElementHelper;
import com.hepsiburada.webAutomation.helper.StoreHelper;
import com.hepsiburada.webAutomation.model.ElementInfo;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

public class BaseMethods {

    private WebDriver driver;
    private FluentWait fluentWait;

    public BaseMethods(WebDriver driver){
        this.driver = driver;
        fluentWait = getFluentWait(30);
    }

    public FluentWait getFluentWait(long second){

        return new FluentWait(driver).withTimeout(Duration.ofSeconds(second)).pollingEvery(Duration.ofMillis(300)).ignoring(NoSuchElementException.class);
    }

    public WebElement findElementKey(String key) {
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        WebElement element = null;
        By by = ElementHelper.getElementInfoToBy(elementInfo);
        try {
            element = (WebElement) fluentWait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("key = %s by = %s Element not found ", key, by.toString() );
        }
        return element;
    }

    public void clickElement(String key){
        findElementKey(key).click();
    }
}
